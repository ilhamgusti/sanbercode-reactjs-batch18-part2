import React from 'react'
import { DarkModeProvider, DataBuahProvider, Routes } from './components/Tugas15';


const App = () => {
    return (
        <DataBuahProvider>
            <DarkModeProvider>
                <Routes/>
            </DarkModeProvider>
        </DataBuahProvider>
    )
}

export default App
