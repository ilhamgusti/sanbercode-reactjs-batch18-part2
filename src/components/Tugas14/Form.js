import React, { useContext } from 'react'
import { ListBuahContext } from './Context'
import axios from 'axios';

const Form = () => {
    // state data tidak dipakai di form, hanya setData saja.
    const [, setData, input, setInput] = useContext(ListBuahContext);
    

    const handleInputName = (e) => {
        const { value } = e.target;
        setInput(prevState=>({ ...prevState,name:value}))
    }
    const handleInputPrice = (e) => {
        const { value } = e.target;
        setInput(prevState=>({ ...prevState,price:value}))
    }
    const handleInputWeight = (e) => {
        const { value } = e.target;
        setInput(prevState=>({ ...prevState,weight:value}))
    }


    const handleSubmit = (e) => {
    e.preventDefault()

    let name = input.name
    let price = input.price.toString()
    

    if (input.id === null){        
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight: input.weight})
          .then(({ data }) => {
          //pemanggilan data menggunakan previous state
          setData(prevState =>[
            ...prevState, 
            { id: data.id, 
              name, 
              price,
              weight: input.weight
            }])
      });
    }else{
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {name, price, weight: input.weight})
      .then(() => {
          setData(prevState => {
            const dataBuah = prevState.find(buah => buah.id === input.id)
            dataBuah.name = name
            dataBuah.price = price
            dataBuah.weight = input.weight
              return[...prevState]
          })
      })
    }

    setInput({name: "", price: "", weight: 0, id: null})


    }
    return (
        <form className="container" onSubmit={handleSubmit}>
            <h1 className="title">
                Form Pembelian Buah
            </h1>
            <div className="form">
                <label htmlFor="namaBuah">Nama Buah</label>
                    <input type="text" id="namaBuah" required  className="input-nama-pelanggan" value={input.name} onChange={handleInputName}/>
                <label htmlFor="hargaBuah">Harga Buah</label>
                    <input type="text" id="hargaBuah" required  className="input-nama-pelanggan" value={input.price} onChange={handleInputPrice}/>
                <label htmlFor="beratBuah">Berat Buah (dalam satuan gram)</label>
                <input type="text" id="beratBuah" required  className="input-nama-pelanggan" value={input.weight} onChange={handleInputWeight}/>
            </div>
            <button>Update</button>
            <button>Cancel</button>
        </form>
    )
}

export default Form
