import React, { useState,useEffect, createContext } from "react";
import axios from 'axios'

export const ListBuahContext = createContext();

export const ListBuahProvider = props => {

    const [data, setData] =  useState([])

    useEffect( () => {
          axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
          .then(({data: resBuah}) => {
            setData(resBuah.map(buah=>{ return {id: buah.id, name: buah.name, price: buah.price, weight: buah.weight }} ))
          })
      }, [])
  
    
    const [input, setInput]  =  useState({name: "", price: "", weight: 0, id: null})
  
    return (
      <ListBuahContext.Provider value={[data, setData, input, setInput]}>
        {props.children}
      </ListBuahContext.Provider>
    );
  };
