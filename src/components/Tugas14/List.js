import axios from 'axios';
import React, { useContext } from 'react';
import { ListBuahContext } from "./Context";

const List = () => {
const [data, setData,,setInput] = useContext(ListBuahContext)
    const handleDelete = (e) => {
        console.log("deleted")
            let idDataBuah = parseInt(e.target.value)

    let newBuah = data.filter(el => el.id !== idDataBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
    .then(({data}) => {
      console.log(data)
    })
    setData([...newBuah])

    }
    const handleEdit = (e) => {
        console.log("edited")
        const {value} = e.target
        const selectedFruit = data.find(x => x.id === parseInt(value))
        
        setInput({name: selectedFruit.name, price: selectedFruit.price, weight: selectedFruit.weight, id:  parseInt(value)})

    }
    return (
        <>
        <div>
            <h1 className="title">Tabel Pembelian Buah</h1>
                <table className="table">
                    {!data.length ? 'tidak ada buah' : <thead className="table-header">
                        <th>No</th>
                        {!!data.length && Object.keys(data[0]).filter(key => key !== 'updated_at' && key !== 'created_at' && key !== 'id').map(key => <th>{key}</th>)}
                        <th>Action</th>
                </thead>}
                
                <tbody>
                    {data.map(({id,name,price,weight},i) => (
                        <tr className="item-buah" key={id}>
                            <td>{i + 1}</td>
                            <td>{name}</td>
                            <td>{price}</td>
                            <td>{weight / 1000} Kg</td>
                            <td><button onClick={handleEdit} value={id}>Edit</button> / <button onClick={handleDelete} value={id}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default List
