import React from "react"
import {ListBuahProvider} from "./Context"
import List from "./List"
import Form from "./Form"
import './../Tugas13/tugas13.css'

const Tugas14 = () =>{
  return (
      <div>
        <ListBuahProvider>
            <Form/>
            <List/>
        </ListBuahProvider>
      </div>
    )
  }
  
  export default Tugas14
