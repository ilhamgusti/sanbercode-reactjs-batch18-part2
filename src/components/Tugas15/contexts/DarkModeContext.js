import React, { useState, createContext } from "react";

export const DarkModeContext = createContext();

export const DarkModeProvider = props => {

    const [color, setColor] = useState('dark')

    const handleChangeColor = () => {
        setColor(prevState => prevState === 'light' ? 'dark' : 'light');
    }
  
    return (
      <DarkModeContext.Provider value={{color, handleChangeColor}}>
        {props.children}
      </DarkModeContext.Provider>
    );
  };
