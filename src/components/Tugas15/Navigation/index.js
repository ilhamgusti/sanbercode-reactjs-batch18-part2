import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { DarkModeContext } from '../contexts';
import './navigation.css';

const Navigation = ({ routesObject }) => {
    const { color, handleChangeColor } = useContext(DarkModeContext);
    console.log({ color });
    return (
        <nav className={`nav ${color}`}>
            <ul>
                {routesObject.map(({ id, link, name }) => (
                   <li key={id}>
                        <Link to={link}>{name}</Link>
                    </li>
                ))}
            </ul> 
            <div>
             mode: <span onClick={handleChangeColor} href="#" style={{cursor:'pointer'}}>{color}</span>
            </div>  
        </nav>
    )
}

export default Navigation
