import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navigation from '../Navigation';
import { routeLists } from '../utils';


const Routes = () => {
    return (
        <Router>
            <Navigation routesObject={routeLists}/>
                <div className="pages">
                    <Switch>
                            {routeLists.map(item => {
                                return <Route path={item.link} component={item.component}/>
                            })}
                    </Switch>
                </div>
        </Router>
    )
}

export default Routes
