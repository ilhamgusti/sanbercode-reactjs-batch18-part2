import {Tugas10, Tugas11, Tugas12, Tugas13, Tugas14, Tugas9} from "../../";

export const routeLists = [
    {
        id: 1,
        link: '/tugas-9',
        name:'Tugas 9',
        component: Tugas9
    },
    {
        id: 2,
        link: '/tugas-10',
        name:'Tugas 10',
        component: Tugas10
    },
    {
        id: 3,
        link: '/tugas-11',
        name:'Tugas 11',
        component: Tugas11
    },
    {
        id: 4,
        link: '/tugas-12',
        name:'Tugas 12',
        component: Tugas12
    },
    {
        id: 5,
        link: '/tugas-13',
        name:'Tugas 13',
        component: Tugas13
    },
    {
        id: 6,
        link: '/tugas-14',
        name:'Tugas 14',
        component: Tugas14
    }
]