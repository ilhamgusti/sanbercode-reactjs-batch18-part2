import React, { useContext } from 'react';
import { DataBuahContext } from '../Tugas15/contexts';
import './Tugas9.css';

const Tugas9 = () => {
    const data = useContext(DataBuahContext)
    return (
        <div className="container">
            <h1 className="title">
                Form Pembelian Buah
      </h1>
            <div className="form">
                <label htmlFor="namaPelanggan">Nama Pelanggan</label>
                <input type="text" id="namaPelanggan" className="input-nama-pelanggan"/>
                <label className="daftar-item">Daftar Item</label>
                <div className="label">
                    {data.map(({nama}) => {
                        return <div>
                            <input type="checkbox" id={nama} />
                            <label htmlFor={nama}>{nama}</label>
                        </div>
                    })}
                </div>
            </div>
            <button>Kirim</button>
        </div>
    );
}

export default Tugas9;
