import React,{Component} from 'react'



class Tugas12 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            daftarBuah: [
                    {nama: "Semangka", harga: 10000, berat: 1000},
                    {nama: "Anggur", harga: 40000, berat: 500},
                    {nama: "Strawberry", harga: 30000, berat: 400},
                    {nama: "Jeruk", harga: 30000, berat: 1000},
                    {nama: "Mangga", harga: 30000, berat: 500}
            ],
            inputNama: "",
            inputHarga: "",
            inputBerat:"",
        }
    }

    handleChangeName = (event) => {
        const { value } = event.target;
        this.setState(prevState=>({ ...prevState,inputNama:value}))
    }
    handleChangeHarga = (event) => {
        const { value } = event.target;
        this.setState(prevState=>({...prevState,inputHarga: value}));
    }
    handleChangeBerat = (event) => {
        const { value } = event.target;
        this.setState(prevState=>({...prevState,inputBerat: value}));
    }

    handleSubmit = (e) =>{
        e.preventDefault()
        this.setState(prevState => ({
            daftarBuah: [
                ...prevState.daftarBuah,
                { nama: prevState.inputNama, harga: prevState.inputHarga, berat: prevState.berat }
            ],
            inputNama: "",
            inputHarga: "",
            inputBerat: "",
        }))
    }

  render() {
    return (
      <div>
        <div>
            <h1 className="title">Tabel Harga Buah</h1>
            <table className="table">
                <thead className="table-header">
                        {Object.keys(this.state.daftarBuah[0]).map((key) => <th key={key}>{key}</th>)}
                        <th>Action</th>
                </thead>
                <tbody>
                    {this.state.daftarBuah.map(({nama,harga,berat},i) => (
                        <tr className="item-buah" key={i}>
                            <td>{nama}</td>
                            <td>{harga}</td>
                            <td>{berat / 1000} Kg</td>
                            <td style={{textAlign:"center"}}><button>edit</button> / <button>delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
            </div>
            <form onSubmit={this.handleSubmit}>
                <label>
                    Masukkan Data Buah:
                </label>          
                <input type="text" value={this.state.inputNama} onChange={this.handleChangeName}/>
                <input type="text" value={this.state.inputBerat} onChange={this.handleChangeBerat}/>
                <input type="text" value={this.state.inputHarga} onChange={this.handleChangeHarga}/>
                <input type="submit" value="Submit" />
            </form>
      </div>
    )
  }
}

export default Tugas12

