import React, { useContext } from 'react'
import { DataBuahContext } from '../Tugas15/contexts';
import './Tugas10.css';

const Tugas10 = () => {
    const data = useContext(DataBuahContext)
    return (
        <div>
            <h1 className="title">Tabel Harga Buah</h1>
            <table className="table">
                <thead className="table-header">
                    {Object.keys(data[0]).map(key => <th>{key}</th>)}
                </thead>
                <tbody>
                    {data.map(({nama,harga,berat}) => (
                        <tr className="item-buah">
                            <td>{nama}</td>
                            <td>{harga}</td>
                            <td>{berat / 1000} Kg</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default Tugas10
