import React, { useEffect, useState } from 'react'
import './tugas13.css'
import axios from 'axios';

const Tugas13 = () => {

    const [data, setData] = useState([
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 }
    ]);

    const [input, setInput] = useState({
        name: "",
        price: "",
        weight: "",
        id:null,
    })

    const initialFetchData = () => {
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
     .then(res => {
         console.log(res)
         setData(res.data)
     })
    }

    useEffect(() => {
        initialFetchData();
    }, [])

    // useEffect(() => {
        
    // }[data])

    const handleInputName = (e) => {
        const { value } = e.target;
        setInput(prevState=>({ ...prevState,name:value}))
    }
    const handleInputPrice = (e) => {
        const { value } = e.target;
        setInput(prevState=>({ ...prevState,price:value}))
    }
    const handleInputWeight = (e) => {
        const { value } = e.target;
        setInput(prevState=>({ ...prevState,weight:value}))
    }
    const handleSubmit = (e) => {
    e.preventDefault()

    let name = input.name
    let price = input.price.toString()
    

    if (input.id === null){        
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight: input.weight})
      .then(({data}) => {
          setData(prevState =>[
            ...prevState, 
            { id: data.id, 
              name, 
              price,
              weight: input.weight
            }])
      });
    }else{
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {name, price, weight: input.weight})
      .then(() => {
          let dataBuah = data.find(el=> el.id === input.id)
          dataBuah.name = name
          dataBuah.price = price
          dataBuah.weight = input.weight
          setData([...data])
      })
    }

    setInput({name: "", price: "", weight: 0, id: null})


    }

    const handleDelete = (e) => {
        console.log("deleted")
            let idDataBuah = parseInt(e.target.value)

    let newBuah = data.filter(el => el.id !== idDataBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
    .then(({data}) => {
      console.log(data)
    })
    setData([...newBuah])

    }
    const handleEdit = (e) => {
        console.log("edited")
        const {value} = e.target
        const selectedFruit = data.find(x => x.id === parseInt(value))
        
        setInput({name: selectedFruit.name, price: selectedFruit.price, weight: selectedFruit.weight, id:  parseInt(value)})

    }
    return (
        <div>
        <form className="container" onSubmit={handleSubmit}>
            <h1 className="title">
                Form Pembelian Buah
            </h1>
            <div className="form">
                <label htmlFor="namaBuah">Nama Buah</label>
                    <input type="text" id="namaBuah" required  className="input-nama-pelanggan" value={input.name} onChange={handleInputName}/>
                <label htmlFor="hargaBuah">Harga Buah</label>
                    <input type="text" id="hargaBuah" required  className="input-nama-pelanggan" value={input.price} onChange={handleInputPrice}/>
                <label htmlFor="beratBuah">Berat Buah (dalam satuan gram)</label>
                <input type="text" id="beratBuah" required  className="input-nama-pelanggan" value={input.weight} onChange={handleInputWeight}/>
            </div>
            <button>Update</button>
            <button>Cancel</button>
        </form>
        <div>
            <h1 className="title">Tabel Pembelian Buah</h1>
                <table className="table">
                    {!data.length ? 'tidak ada buah' : <thead className="table-header">
                        <th>No</th>
                        {!!data.length && Object.keys(data[0]).filter(key => key !== 'updated_at' && key !== 'created_at' && key !== 'id').map(key => <th>{key}</th>)}
                        <th>Action</th>
                </thead>}
                
                <tbody>
                    {data.map(({id,name,price,weight},i) => (
                        <tr className="item-buah" key={id}>
                            <td>{i + 1}</td>
                            <td>{name}</td>
                            <td>{price}</td>
                            <td>{weight / 1000} Kg</td>
                            <td><button onClick={handleEdit} value={id}>Edit</button> / <button onClick={handleDelete} value={id}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
        </div>
    )
}

export default Tugas13
