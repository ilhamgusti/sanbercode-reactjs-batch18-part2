import React, { Component } from 'react'

class Tugas11 extends Component{
  constructor(props){
    super(props)
    this.state = {
        time: 20,
        clock: new Date().toLocaleTimeString(),
        isHidden: false,
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
      }
      if (this.props.clock !== undefined) {
          this.setState({clock:this.props.clock})
      }
        this.timerID = setInterval(
        () => this.tick(),
        1000
        );    
      this.clockId = setInterval(() => this.tok(), 1000);
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
    clearInterval(this.clockId);
  }

tick() {
    if (this.state.time > 0) {
        this.setState(prevState => ({
             time: prevState.time - 1 
        }));
    } else {
        clearInterval(this.timerID)
        this.setState(prevState => ({
            ...prevState,
            isHidden:true,
        }))
    }
  }
  tok() {
      this.setState(prevState => ({
          ...prevState,
          clock: new Date().toLocaleTimeString()
      }));
  }

  render(){
    return(
        <div style={{display:"flex"}}>
            {!this.state.isHidden && (<>
                <h1 style={{marginRight:20}}>Sekarang jam: {this.state.clock}</h1>
                <h1 style={{textAlign: "center"}}>
                    Hitung mundur: {this.state.time}
                </h1>
            </>)}
        
      </div>
    )
  }
}

export default Tugas11
